module.exports = function (grunt) {

    grunt.registerTask('default', ['bower_concat']);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
            all: {
                files: {
                    "public/css/example.css": "public/css/example.less"
                },
                options: {
                    dumpLineNumbers: "all"
                }
            }
        },
        watch: {
            less: {
                files: 'public/css/*.less',
                tasks: ['less']
            }
        },
        bower_concat: {
            all: {
                dest: 'public/js/ext.js',
                dependencies: {
                    'angular': 'jquery'
                },
                mainFiles: {
                    'cropper': [
                        "src/cropper.css",
                        "src/cropper.js"
                    ],
                    'ace-builds': [
                        'src-noconflict/ace.js',
                        'src-noconflict/mode-html.js'
                    ],
                    'datatables-plugins': [
                        'integration/bootstrap/3/dataTables.bootstrap.js'
                    ]
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-bower-concat');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
};
