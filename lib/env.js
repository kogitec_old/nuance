var env = {};
env.http = {
    port: process.env.PORT || 3000
};

env.hostname = process.env.HOSTNAME || ('http://localhost:' + env.http.port);

env.mongo = {
    host: process.env.MONGO_HOST || 'localhost',
    port: process.env.MONGO_PORT || 27017,
    database: process.env.MONGO_DATABASE || 'substance'
};

env.mongo.url = process.env.MONGO_URL || ('mongodb://' + env.mongo.host + ':' + env.mongo.port + '/' + env.mongo.database);

module.exports = env;
