var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var User = require('./models/user');
//var facebook = require('./facebook');

exports.init = function(app) {
	app.use(passport.initialize());
	app.use(passport.session());

	app.post('/login', login);
	app.get('/logout', logout);
	app.get('/currentUser', currentUser);

	app.post('/register', register);
	app.get('/unique/username/:username', uniqueUsername);
	
//	facebook.init(app);
};

var login = function(req, res, next) {
	passport.authenticate('local', {
		session : true
	}, function(err, user, info) {
		if (err) return next(err);
		if (!user) {
			res.statusCode = 401;
			return res.send("Nom d'utilisateur ou mot de passe incorrect");
		}
		req.logIn(user, function(err) {
			if (err) return next(err);
			return res.send(user);
		});
	})(req, res, next);
};

var logout = function(req, res, next) {
	req.logout();
	res.writeHead(302, {
		'Location' : '/'
	});
	res.end();
};

var currentUser = function(req, res, next) {
	res.send(req.user);
};

var register = function(req, res, next) {
	if (req.body._id) {
		User.findById(req.body._id, function(err, user) {
			if (err) return next(err);
			user.email = req.body.email;
			user.username = req.body.username;
			user.save(function(err, user) {
				if (err) return next(err);
				res.send(user);
			});
		});
	}
	else {
		new User(req.body).save(function(err, user) {
			if (err) return next(err);
			res.send(user);
		});
	}
};

var uniqueUsername = function(req, res, next) {
	username = req.params.username;
	User.findOne({
		username : username
	}, function(err, user) {
		if (err)
			return next(err);
		res.send({
			taken : user !== null
		});
	});
};

passport.use(new LocalStrategy(function(username, password, done) {
	User.findOne({
		username : username
	}, function(err, user) {
		if (err)
			return done(err);
		if (!user) {
			return done(null, false, {
				message : 'Incorrect username'
			});
		}
		if (!user.checkPassword(password)) {
			return done(null, false, {
				message : 'Incorrect password'
			});
		}
		return done(null, user);
	});
}));

passport.serializeUser(function(user, done) {
	done(null, user._id);
});

passport.deserializeUser(function(_id, done) {
	User.findById(_id, function(err, user) {
		done(err, user);
	});
});