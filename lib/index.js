var fs = require('fs');

var scripts = [
    'js/ext.js',
    'js/app.js',
    'js/security.js',
    'js/datatable.js',
    'js/misc/find.js'
];

exports.init = function (app) {
    push('js/' + app.options.name + '.js');

    app.get('/', function (req, res) {
        res.render('index', {
            name : app.options.name,
            title: app.options.title,
            scripts: scripts
        });
    });
};

var walk = function(root, dir) {
    fs.readdir(dir, function(err, list) {
        if (err) throw err;
        list.forEach(function(file) {
            file = dir + '/' + file;
            var stat = fs.statSync(file);
            if (stat && stat.isDirectory()) {
                walk(root, file);
            } else {
                if (file.indexOf('.js', file.length - 3) !== -1) {
                    push(file.substring(root.length+1));
                }
            };
        });
    });
};

walk('public', 'public');

var push = function(script) {
    var present = false;
    for (var i = 0; i < scripts.length; i++) {
        if (scripts[i] == script) present = true;
    }
    if (!present) {
        scripts.push(script);
    }
}