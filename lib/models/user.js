var mongoose = require('mongoose');

var User = mongoose.model('User', mongoose.Schema({
    username: String,
    password: String,
    firstName: String,
    lastName: String,
    facebook: {
        id: String,
        accessToken: String,
        refreshToken: String
    }
}));

User.prototype.checkPassword = function (password) {
    return this.password === password;
};

module.exports = User;