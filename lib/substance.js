require('./find');
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var multiparty = require('connect-multiparty');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var debug = require('debug')('assets');
var env = require('./env');

function createApplication(options) {
    var app = express();

    app.options = options;

    if (options.schemas) {
        loadSchemas(app, options.schemas);
    }
    // Base plugins
    app.use(logger('dev'));
    app.use(express.static('public'));
    app.use(express.static(__dirname + '/../public'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: false}));
    app.use(cookieParser());
    app.use(multiparty());
    app.use(require('express-session')({
        secret: 'keyboard cat',
        resave: true,
        saveUninitialized: true
    }));

    // Register views
    app.set('views', __dirname + '/../public');
    app.set('view engine', 'ejs');

    // Init index view
    require('./index').init(app);
    require('./security').init(app);

    // Call application init function
    if (options && options.init) {
        options.init(app);
    }

    // 404
    app.use(function (req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    // 500 (dev)
    if (app.get('env') === 'development') {
        app.use(function (err, req, res, next) {
            res.status(err.status || 500);
            res.send({
                message: err.message,
                error: err
            });
        });
    }

    // 500 (prod)
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.send({
            message: err.message,
            error: {}
        });
    });

    // Add start function
    app.start = function () {
        startMongo();
        startHttp(this);
    }

    return app;
}

var startMongo = function (app) {
    console.log("Connecting to Mongo (" + env.mongo.url + ")");
    mongoose.connect(env.mongo.url, null, function (err) {
        if (err) {
            console.log(err);
        }
        else {
            console.log('Connected to mongo successfully');
        }
    });
};

var startHttp = function (app) {
    console.log("Starting http server on port " + env.http.port);
    var server = app.listen(env.http.port, function () {
        console.log('Substance application listening on port ' + server.address().port);
    });
};

var loadSchemas = function (app, schemas) {
    for (var schema in schemas) {
        app[schema] = mongoose.model(schema, mongoose.Schema(schemas[schema]));
    }
};

exports = module.exports = createApplication;
