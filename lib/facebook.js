var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;
var https = require('https');

var User = require('./models/user');

exports.init = function(mazoos) {
	mazoos.get('/facebook', passport.authenticate('facebook', {
		scope : ['email', 'user_photos']
	}));
	mazoos.get('/facebook/callback', passport.authenticate('facebook', {
		successRedirect : '/#/',
		failureRedirect : '/#/'
	}));
	mazoos.get('/facebook/info', facebookInfo);
};

var facebookInfo = function(req, res, next) {
	if (req.user && req.user.facebook) {
		var facebook = req.user.facebook;
		https.get({
			method : 'GET',
			hostname : 'graph.facebook.com',
			path : '/me?access_token=' + facebook.accessToken + "&fields=picture,name,email"
		}, function(response) {
			var str = '';
			response.on('data', function(chunk) {
				str += chunk;
			});
			response.on('end', function() {
				res.send(str);
			});
		}).end();
	} else {
		res.end();
	}
};

passport.use(new FacebookStrategy({
	clientID : process.env.FACEBOOK_CLIENT_ID,
	clientSecret : process.env.FACEBOOK_CLIENT_SECRET,
	callbackURL : process.env.HOSTNAME + '/facebook/callback'
}, function(accessToken, refreshToken, profile, done) {
	User.findOne({
		'facebook.id' : profile.id
	}, function(err, user) {
		if (err) {
			return done(err);
		}
		if (!user) {
			user = new User({
				facebook : {
					id : profile.id,
					accessToken : accessToken,
					refreshToken : refreshToken
				},
				status : 'unregistered'
			});
			user.save(function(err) {
				if (err)
					return done(err);
				done(null, user);
			});
		}
		done(null, user);
	});
}));