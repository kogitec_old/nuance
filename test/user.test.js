var assert = require("assert");

var app = require("../substance")({
    name:'example',
    title:'Example',
    init: function(app) {
    }
});

describe('Substance', function(){
    describe('init', function(){
        it('should have right name and title', function(){
            assert.equal('example', app.options.name);
        })
    })
});
