var app = angular.module('app', ['ui.router', 'ui.bootstrap', 'datatables']);

app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.when('', '/');
    $stateProvider.state('settings', {
        url: '/settings',
        templateUrl: 'partials/settings.html'
    });
}]);

app.run(function ($rootScope, $state, $stateParams, $sce) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.html = function (texte) {
        return $sce.trustAsHtml(texte);
    };
});

$(function () {
    $("body").niceScroll();
});