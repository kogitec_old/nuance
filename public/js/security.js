app.controller('Security', function($http, $rootScope, $scope, $modal, $location) {
	
	$http({
		method : 'GET',
		url : '/currentUser'
	}).success(function(user) {
		$rootScope.user = user;
		if (user) {
			if (user.facebook) {
				$http({
					method : 'GET',
					url : '/facebook/info'  
				}).success(function(info) {
					$rootScope.facebookInfo = info;
					if (!user.username) {
						$modal.open({
							templateUrl : 'partials/security/register.html',
							controller : 'Register',
							size : 'sm',
							backdrop : 'static',
							keyboard : false
						});			
					}
				});
			}			
		}
	});
	
	$scope.login = function() {
		var modalInstance = $modal.open({
			templateUrl : 'partials/security/login.html',
			controller : 'Login',
			size : 'sm'
		});
	};

	$scope.register = function() {
		var modalInstance = $modal.open({
			templateUrl : 'partials/security/register.html',
			controller : 'Register',
			size : 'sm'
		});
	};

	$scope.logout = function() {
		$http({
			method : 'GET',
			url : '/logout'
		}).success(function(data) {
			$rootScope.user = null;
			$rootScope.facebookInfo = null;
			$location.path('');
		});
	};
});

app.controller('Login', function($rootScope, $scope, $http, $modalInstance) {
	$scope.credentials = {};
	
	$scope.login = function() {
		$http({
			method : 'POST',
			url : '/login',
			data : $scope.credentials
		}).success(function(user) {
			$rootScope.user = user;
			$modalInstance.dismiss();
		}).error(function(data) {
			$scope.message = data;
		});
	};
	
	$scope.cancel = function() {
		$modalInstance.dismiss();
	}
});

app.controller('Register', function($rootScope, $scope, $http, $modalInstance) {
	$scope.info = {
		_id : $rootScope.user ? $rootScope.user._id : null,
		email : $rootScope.facebookInfo ? $rootScope.facebookInfo.email : null
	};
	
	$scope.visited = {};
	$scope.errors = {};

	$scope.register = function() {
		$http({
			method : 'POST',
			url : '/register',
			data : $scope.info
		}).success(function(user) {
			$rootScope.user = user;
			$modalInstance.dismiss();
		}).error(function(data) {
			$scope.message = data;
		});
	};
	
});

app.directive("passwordVerify", function() {
	return {
		restrict : 'A',
		require : "ngModel",
		link : function(scope, element, attrs, ctrl) {
			
			var password = null;
			scope.$watch(attrs.passwordVerify, function(value) {
				password = value;
				ctrl.$validate();
			});
			ctrl.$validators.passwordVerify = function(value) {
				return ctrl.$isEmpty(value) || value == password;
			};
		}		
	};
});

app.directive("uniqueUsername", function($http) {
	return {
		restrict : 'A',
		require : "ngModel",
		link : function(scope, element, attrs, ctrl) {
			var taken = false;
			scope.$watch(function() {
				return ctrl.$modelValue;
			}, function(newValue, oldValue) {
				if (newValue) {
					$http({
						method : 'GET',
						url : '/unique/username/' + newValue
					}).success(function(data) {
						return ctrl.$setValidity('uniqueUsername', !data.taken);
					});
				}
			});
			ctrl.$validators.uniqueUsername = function(value) {
				return ctrl.$isEmpty(value) || !taken;
			};
		}		
	};
});