app.directive('datatable', function ($http) {
    return {
        restrict: 'E',
        scope: {
            data: '=data',
            options: '=options'
        },
        templateUrl: 'partials/directive/datatable.html',
        link: function (scope, element, attrs) {
            scope.page = 1;
            scope.pageSize = 10;
            scope.total = 0;

            scope.pageButtons = [1];

            scope.$watch('page', function() {
                $http({
                    method: 'GET',
                    url: scope.options.url,
                    params: {
                        skip: (scope.page-1) * scope.pageSize,
                        limit : scope.pageSize
                    }
                }).success(function (res) {
                    scope.total = res.total;
                    scope.data = res.data;
                });
            });

            scope.go = function(page) {
                scope.page = page;
            };

            scope.$watch('data', function() {
                var range = 2;
                var pages = scope.total / scope.pageSize;

                var min = scope.page - range;
                if (min < 1) min = 1;

                var max = scope.page + range;
                if (max > pages) max = pages;

                scope.pageButtons = [];
                for (var i = min; i <= max; i++) {
                    scope.pageButtons.push(i);
                }
            })
        }
    };
});