var findFunction = function (condition) {
    var list = Object(this);
    var key = Object.keys(condition)[0];
    var value = condition[key];
    var length = list.length < 0 ? 0 : list.length >>> 0; // ES.ToUint32;
    if (length === 0) return undefined;
    for (var i = 0, obj; i < length; i++) {
        obj = list[i];
        if (obj[key] == value) return obj;
    }
    return undefined;
};

findFunction.value = function() {};

Array.prototype.find = findFunction;