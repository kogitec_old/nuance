app.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('example', {
        url: '/',
        templateUrl: 'partials/example.html'
    });
}]);

app.run(['$rootScope', function ($rootScope) {
    $rootScope.title = 'Example';
    $rootScope.menus = [{
        title: 'Example',
        route: 'example'
    }];
}]);